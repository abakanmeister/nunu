import Vuex from 'vuex';
import Cookie from 'js-cookie';

const createStore = () => {
    return new Vuex.Store({
        state: {
            example: [],
            token: null
        },
        mutations: {
            example(state, example) {
                state.example = example;
            }
        },
        actions: {
            // nuxtServerInit(vuexContext, context) {
            //     return context.app.$axios
            //         .$get('')
            //         .then(data => {
            //         
            //         })
            //         .catch(e => context.error(e))
            // },
            getUserInfo(vuexContext, data){
                return this.$axios
                    .$get('/account/profile/' + data.id)
                    .then(result => {
                        console.log(result)
                    }).catch(e => console.log(e));
            },
            authenticateUser(vuexContext, authData) {
                let authUrl = 'http://news.re19.ru/api/mobile/lastnews';
                // let authUrl = '/signin';
                if (!authData.isLogin) {
                    // authUrl = '/signup';
                }
                return this.$axios
                    .$get(authUrl, {
                        email: authData.email,
                        password: authData.password
                        // returnSecureToken: true,
                    }).then(result => {
                        console.log(result)

                }).catch(e => console.log(e));
            },
        
            initAuth(vuexContext, req) {
                let token,
                    expirationDate;
                if (req) {
                    if (!req.headers.cookie) {
                        return;
                    }
                    const jwtCookie = req.headers.cookie
                        .split(';')
                        .find(c => c.trim().startsWith('jwt='));
                    if (!jwtCookie) {
                        return;
                    }
                    token = jwtCookie.split('=')[1];
                    expirationDate = req.headers.cookie
                        .split(';')
                        .find(c => c.trim().startsWith('expirationDate='))
                        .split('=')[1];
                } else {
                    token = localStorage.getItem('token');
                    expirationDate = localStorage.getItem('tokenExpiration');
                }
                if (new Date().getTime() > +expirationDate || !token) {
                    console.log('no token or invalid token')
                    vuexContext.dispatch('logout');
                    return;
                }
                vuexContext.commit('setToken', token);
            },
            logout(vuexContext) {
                if (process.client) {
    
                }
            }
        },
        getters:{
            isAuthenticated(state) {
                return state.token != null
            }
        }
    });
};

export default createStore;